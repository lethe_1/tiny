package org.tinygroup.jdbctemplatedslsession;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;
import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.jdbctemplatedslsession.provider.DefaultTableMetaDataProvider;
import org.tinygroup.jdbctemplatedslsession.rowmapper.SimpleRowMapperSelector;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.tinysqldsl.ComplexSelect;
import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.DslSession;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;
import org.tinygroup.tinysqldsl.Update;
import org.tinygroup.tinysqldsl.base.InsertContext;

/**
 * DslSqlSession接口的jdbctemplate版实现
 * 
 * @author renhui
 * 
 */
public class SimpleDslSession implements DslSession {

	private JdbcTemplate jdbcTemplate;
	private TableMetaDataProvider provider;
	private DataFieldMaxValueIncrementer incrementer;
	private RowMapperSelector selector = new SimpleRowMapperSelector();
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SimpleDslSession.class);

	public SimpleDslSession(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		provider = new DefaultTableMetaDataProvider();
	}

	public SimpleDslSession(DataSource dataSource,
			DataFieldMaxValueIncrementer incrementer) {
		this(dataSource);
		this.incrementer = incrementer;
	}

	public RowMapperSelector getSelector() {
		return selector;
	}

	public DataFieldMaxValueIncrementer getIncrementer() {
		return incrementer;
	}

	public void setIncrementer(DataFieldMaxValueIncrementer incrementer) {
		this.incrementer = incrementer;
	}

	public void setSelector(RowMapperSelector selector) {
		this.selector = selector;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public int execute(Insert insert) {
		logMessage(insert.sql(), insert.getValues());
		return jdbcTemplate.update(insert.sql(), insert.getValues().toArray());
	}

	@SuppressWarnings("unchecked")
	public <T> T executeAndReturnObject(Insert insert) {
		Class<T> pojoType = insert.getContext().getTable().getPojoType();
		return executeAndReturnObject(insert, pojoType);
	}

	public <T> T executeAndReturnObject(Insert insert, Class<T> clazz) {
		return executeAndReturnObject(insert, clazz, true);
	}

	public <T> T executeAndReturnObject(Insert insert, Class<T> clazz,
			boolean autoGeneratedKeys) {
		if (clazz == null) {
			throw new IllegalArgumentException(
					"The type argument can not be empty");
		}
		InsertContext context = insert.getContext();
		TableMetaData metaData = provider.generatedKeyNamesWithMetaData(
				jdbcTemplate.getDataSource(), context.getSchema(), null,
				context.getTableName());
		ObjectMapper mapper = new ObjectMapper(clazz);
		mapper.setDslSession(this);
		return mapper.assemble(autoGeneratedKeys, metaData, insert);
	}

	private void logMessage(String sql, List<Object> values) {
		LOGGER.logMessage(LogLevel.DEBUG, "Executing SQL:[{0}],values:{1}",
				sql, values);
	}

	public int execute(Update update) {
		logMessage(update.sql(), update.getValues());
		return jdbcTemplate.update(update.sql(), update.getValues().toArray());
	}

	public int execute(Delete delete) {
		logMessage(delete.sql(), delete.getValues());
		return jdbcTemplate.update(delete.sql(), delete.getValues().toArray());
	}

	@SuppressWarnings("unchecked")
	public <T> T fetchOneResult(Select select, Class<T> requiredType) {
		logMessage(select.sql(), select.getValues());
		return (T) jdbcTemplate.queryForObject(select.sql(), select.getValues()
				.toArray(), selector.rowMapperSelector(requiredType));
	}

	@SuppressWarnings("unchecked")
	public <T> T[] fetchArray(Select select, Class<T> requiredType) {
		List<T> records = fetchList(select, requiredType);
		if (!CollectionUtil.isEmpty(records)) {
			return (T[]) records.toArray();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> fetchList(Select select, Class<T> requiredType) {
		logMessage(select.sql(), select.getValues());
		return jdbcTemplate.query(select.toString(), select.getValues()
				.toArray(), selector.rowMapperSelector(requiredType));
	}

	@SuppressWarnings("unchecked")
	public <T> T[] fetchArray(ComplexSelect complexSelect, Class<T> requiredType) {
		List<T> records = fetchList(complexSelect, requiredType);
		if (!CollectionUtil.isEmpty(records)) {
			return (T[]) records.toArray();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> fetchList(ComplexSelect complexSelect,
			Class<T> requiredType) {
		logMessage(complexSelect.sql(), complexSelect.getValues());
		return jdbcTemplate.query(complexSelect.toString(), complexSelect
				.getValues().toArray(), selector
				.rowMapperSelector(requiredType));
	}

	@SuppressWarnings("unchecked")
	public <T> T fetchOneResult(ComplexSelect complexSelect,
			Class<T> requiredType) {
		logMessage(complexSelect.sql(), complexSelect.getValues());
		return (T) jdbcTemplate.queryForObject(complexSelect.sql(),
				complexSelect.getValues().toArray(),
				selector.rowMapperSelector(requiredType));
	}
}
