package org.tinygroup.context2object.test.bean;

public class BeanNoField {
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return "BeanField";
	}
}
